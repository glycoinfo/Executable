GlycanBuilder2

* GlycanBuilder2_for_Win_x86 Supporting SNFG  [GlycanBuilder2_for_Win_x86.zip] 

* GlycanBuilder2_for_mac Supporting SNFG [glycanbuilder2_mac.zip] --> move to https://gitlab.com/GlycoTool/dev-version/-/tree/master/GlycanBuilder/mac
* GlycanBuilder2_for_Windows x64 Supporting SNFG [Glycanbuilder2_winx64.zip] --> move to https://gitlab.com/GlycoTool/dev-version/-/tree/master/GlycanBuilder/win

Release Note:

* 2021-12-02: GlycanBuilder2_for_win 1.14.0

* 2021-12-01: GlycanBuilder2_for_mac 1.14.0

